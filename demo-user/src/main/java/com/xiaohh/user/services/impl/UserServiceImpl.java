package com.xiaohh.user.services.impl;

import com.xiaohh.user.entities.User;
import com.xiaohh.user.mappers.UserMapper;
import com.xiaohh.user.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * user 对象的业务处理层实现类
 * </p>
 *
 * @author XiaoHH
 * @version 1.0
 * @date 2021-02-22 星期一 20:08:46
 * @file UserServiceImpl.java
 */
@Service
public class UserServiceImpl implements UserService {

    /**
     * user 对象的业务访问层
     */
    @Autowired
    private UserMapper userMapper;

    /**
     * 查询所有的用户对象并返回
     *
     * @return 用户列表
     */
    @Override
    public List<User> list() {
        return this.userMapper.list();
    }

    /**
     * 添加一个用户
     *
     * @param user 用户对象
     * @return 是否成功
     */
    @Override
    public Boolean add(User user) {
        return this.userMapper.insert(user) > 0;
    }

    /**
     * 根据 id 获取用户对象
     *
     * @param id 用户 id
     * @return 用户对象
     */
    @Override
    public User get(long id) {
        return this.userMapper.get(id);
    }

    /**
     * 更新用户对象，用户对象的 id 不能为空
     *
     * @param user 用户对象
     * @return 是否成功
     */
    @Override
    public boolean update(User user) {
        return this.userMapper.update(user) > 0;
    }

    /**
     * 根据用户 id 删除用户
     *
     * @param id 用户 id
     * @return 是否成功
     */
    @Override
    public boolean delete(long id) {
        return this.userMapper.delete(id) > 0;
    }
}
