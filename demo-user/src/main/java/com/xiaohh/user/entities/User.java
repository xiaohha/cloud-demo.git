package com.xiaohh.user.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xiaohh.user.utils.ValidateGroup;
import lombok.*;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 对应数据库中的 user 表
 * </p>
 *
 * @author XiaoHH
 * @version 1.0
 * @date 2021-02-22 星期一 19:23:56
 * @file User.java
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Accessors(chain = true)
public class User implements Serializable {

    /**
     * 用户 ID
     */
    @NotNull(groups = {ValidateGroup.UpdateGroup.class}, message = "用户id不能为空")
    @Null(groups = {ValidateGroup.AddGroup.class}, message = "用户id必须为空")
    private Long id;

    /**
     * 用户名
     */
    @NotNull(groups = {ValidateGroup.AddGroup.class}, message = "用户名不能为空")
    private String username;

    /**
     * 密码
     */
    @NotNull(groups = {ValidateGroup.AddGroup.class}, message = "密码不能为空")
    private String password;

    /**
     * 出生日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd") // 前端传过来的数据格式
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8") // 我们传给前端的数据格式
    private Date birthday;

    /**
     * 计算年龄
     *
     * @return 年龄
     */
    public int getAge() {
        if (this.birthday == null) return 0;
        // 现在的日期
        Date now = new Date();

        // 初始计算年龄
        int age = now.getYear() - this.birthday.getYear();

        // 设置成同年份然后进行比较
        now.setYear(this.birthday.getYear());
        // 将时间设置成当天的 0:00:00.000
        now.setTime(now.getTime() - (now.getTime() % (1000 * 60 * 60 * 24)) - 1000 * 60 * 60 * 8);
        // this.birthday.setTime(this.birthday.getTime() - (this.birthday.getTime() % (1000 * 60 * 60 * 24)) - 1000 * 60 * 60 * 8);
        /*
        this.birthday.compareTo(now);
        如果 birthday 比 now 大，则返回1，如果小返回-1（代表生日还没过），相等则返回0
         */
        age += this.birthday.compareTo(now);

        return age;
    }
}
