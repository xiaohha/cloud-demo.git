package com.xiaohh.user.utils;

import com.xiaohh.user.constants.BizCodeEnum;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 所有 controller 的返回对象，用于封装返回数据
 * </p>
 *
 * @author XiaoHH
 * @version 1.0
 * @date 2021-02-22 星期一 20:02:23
 * @file R.java
 */
public class R extends HashMap<String, Object> implements Serializable {

    /**
     * R 对象唯一的构造方法
     *
     * @param code    响应 code
     * @param message 响应的消息
     */
    private R(int code, String message) {
        super.put("code", code);
        super.put("message", message);
    }

    /**
     * 表示成功的方法
     *
     * @return 响应对象
     */
    public static R ok() {
        return R.ok(0, "success");
    }

    /**
     * 表示成功的方法
     *
     * @param code 响应 code
     * @return 响应对象
     */
    public static R ok(int code) {
        return R.ok(code, "success");
    }

    /**
     * 表示成功的方法
     *
     * @param message 响应消息
     * @return 响应对象
     */
    public static R ok(String message) {
        return R.ok(0, message);
    }

    /**
     * 表示成功的方法
     *
     * @param code    响应 code
     * @param message 响应消息
     * @return 响应对象
     */
    public static R ok(int code, String message) {
        return new R(code, message);
    }

    /**
     * 表示失败的方法
     *
     * @return 响应对象
     */
    public static R error() {
        return R.error(500, "error");
    }

    /**
     * 表示失败的方法
     *
     * @param code 响应 code
     * @return 响应对象
     */
    public static R error(int code) {
        return R.error(code, "error");
    }

    /**
     * 表示失败的方法
     *
     * @param message 响应消息
     * @return 响应对象
     */
    public static R error(String message) {
        return R.error(500, message);
    }

    /**
     * 表示失败的方法
     *
     * @param code    响应 code
     * @param message 响应消息
     * @return 响应对象
     */
    public static R error(int code, String message) {
        return new R(code, message);
    }

    /**
     * 表示失败的方法
     *
     * @param codeEnum 错误描述的枚举
     * @return 响应对象
     */
    public static R error(BizCodeEnum codeEnum) {
        return new R(codeEnum.getCode(), codeEnum.getMessage());
    }

    /**
     * 添加一个参数
     *
     * @param key   参数的 key
     * @param value 参数的 value
     * @return 链式调用
     */
    public R put(String key, Object value) {
        super.put(key, value);
        return this;
    }

    /**
     * 添加多个参数
     *
     * @param params 参数集合
     * @return 链式调用
     */
    public R put(Map<String, Object> params) {
        super.putAll(params);
        return this;
    }

    /**
     * 添加一个数据对象，键为 data
     *
     * @param data data 对象
     * @return 链式调用
     */
    public static R data(Object data) {
        return R.ok().put("data", data);
    }

    /**
     * 根据定义返回一个响应对象，如果定义是 true，则返回 ok，否则返回 error
     *
     * @param result 定义
     * @return 响应对象
     */
    public static R condition(boolean result) {
        return result ? R.ok() : R.error();
    }

    /**
     * 根据定义返回一个响应对象，如果定义是 true，则返回 ok，否则返回 error
     *
     * @param result  定义
     * @param code    响应 code
     * @param message 响应消息
     * @return 响应对象
     */
    public static R condition(boolean result, int code, String message) {
        return result ? R.ok(code, message) : R.error(code, message);
    }

    /**
     * 根据定义返回一个响应对象，如果定义是 true，则返回 ok，否则返回 error
     *
     * @param result 定义
     * @param code   响应 code
     * @return 响应对象
     */
    public static R condition(boolean result, int code) {
        return result ? R.ok(code) : R.error(code);
    }

    /**
     * 根据定义返回一个响应对象，如果定义是 true，则返回 ok，否则返回 error
     *
     * @param result  定义
     * @param message 响应消息
     * @return 响应对象
     */
    public static R condition(boolean result, String message) {
        return result ? R.ok(message) : R.error(message);
    }

    /**
     * 根据定义返回一个响应对象，如果定义是 true，则返回 ok，否则返回 error
     * @param result 定义
     * @param trueMessage 为真的时候的响应消息
     * @param falseMessage 为假的时候的响应消息
     * @return 响应对象
     */
    public static R condition(boolean result, String trueMessage, String falseMessage) {
        return result ? R.ok(trueMessage) : R.error(falseMessage);
    }
}
