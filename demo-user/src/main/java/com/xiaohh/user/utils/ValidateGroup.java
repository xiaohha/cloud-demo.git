package com.xiaohh.user.utils;

/**
 * <p>
 * 分组校验的包装类
 * </p>
 *
 * @author XiaoHH
 * @version 1.0
 * @date 2021-03-11 星期四 21:00:03
 * @file ValidateGroup.java
 */
public class ValidateGroup {

    /**
     * 添加数据的分组
     */
    public interface AddGroup {
    }

    /**
     * 更新数据的分组
     */
    public interface UpdateGroup {
    }
}
