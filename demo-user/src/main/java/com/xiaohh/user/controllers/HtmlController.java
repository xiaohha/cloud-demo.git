package com.xiaohh.user.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p></p>
 *
 * @author tanghai
 * @version 1.0
 * @date 2021-08-17 星期二 15:01:07
 */
@Controller
@RequestMapping("/static")
public class HtmlController {

    @RequestMapping("/{html}")
    public String html(@PathVariable("html") String html) {
        return html;
    }
}
