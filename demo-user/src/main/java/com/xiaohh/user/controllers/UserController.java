package com.xiaohh.user.controllers;

import com.xiaohh.user.entities.User;
import com.xiaohh.user.services.UserService;
import com.xiaohh.user.utils.R;
import com.xiaohh.user.utils.ValidateGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *     user 对象的前端控制类
 * </p>
 *
 * @author XiaoHH
 * @version 1.0
 * @date 2021-02-22 星期一 20:05:36
 * @file UserController.java
 */
@RestController
@RequestMapping("/v1/user")
public class UserController {

    /**
     * 用户对象的业务处理类
     */
    @Autowired
    private UserService userService;

    /**
     * 向数据库中添加一个数据的 Controller 接口
     * @param user 用户对象
     * @return 返回信息中包含是否成功
     */
    @PostMapping
    public R add(@Validated(ValidateGroup.AddGroup.class) @RequestBody User user) {
        return R.condition(this.userService.add(user), "用户添加成功", "用户添加成功添加失败");
    }

    /**
     * 用户对象列表查询
     * @return 用户对象的列表
     */
    @GetMapping
    public R list() {
        return R.ok("用户对象列表查询成功").put("data", this.userService.list());
    }

    /**
     * 根据 id 获取用户对象
     * @param id 用户 id
     * @return 用户对象
     */
    @GetMapping("/{id}")
    public R get(@PathVariable("id") long id) {
        return R.ok("用户对象查询成功").put("data", this.userService.get(id));
    }

    /**
     * 根据用户 id 更新一个用户对象
     * @param user 用户对象
     * @return 是否成功
     */
    @PutMapping
    public R update(@Validated(ValidateGroup.UpdateGroup.class) @RequestBody User user) {
        return R.condition(this.userService.update(user), "用户更新成功", "用户更新成功");
    }

    /**
     * 根据用户 id 删除用户对象
     * @param id 用户 id
     * @return 是否成功
     */
    @DeleteMapping("/{id}")
    public R delete(@PathVariable("id") long id) {
        return R.condition(this.userService.delete(id), "用户删除成功", "用户删除失败");
    }
}
