SET NAMES `UTF8`;
DROP DATABASE IF EXISTS `demo_user`;
CREATE DATABASE `demo_user`;
USE `demo_user`;

-- 创建用户表
CREATE TABLE `user` (
    `id` BIGINT PRIMARY KEY AUTO_INCREMENT COMMENT '用户 ID',
    `username` VARCHAR(20) NOT NULL COMMENT '用户名',
    `password` VARCHAR(20) NOT NULL COMMENT '密码',
    `birthday` DATE COMMENT '出生日期'
);